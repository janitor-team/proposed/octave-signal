Source: octave-signal
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Rafael Laboissière <rafael@debian.org>,
           Mike Miller <mtmiller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-octave (>= 0.7.1),
               octave-control (>= 3.1.0)
Build-Conflicts: octave-nan
Standards-Version: 4.5.0
Homepage: https://octave.sourceforge.io/signal/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-signal.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-signal
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-signal
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${octave:Depends}
Description: signal processing functions for Octave
 This package provides signal processing tools including filtering,
 windowing and display functions in octave.
 .
 Blackman-Harris, Blackman-Nuttall and Bohman are among the windowing functions
 and filters include Chebyshev type filters and butterworth filters.
 .
 Additionally, some wavelet functions are included.
 .
 This Octave add-on package is part of the Octave-Forge project.
